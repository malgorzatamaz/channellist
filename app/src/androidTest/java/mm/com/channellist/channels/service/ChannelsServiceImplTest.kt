package mm.com.channellist.channels.service

import android.support.test.InstrumentationRegistry
import mm.com.channellist.channels.listeners.OnChannelsLoaded
import mm.com.channellist.channels.model.Channel
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.util.concurrent.CountDownLatch

class ChannelsServiceImplTest {

    private lateinit var service: ChannelsService
    private lateinit var doneSignal: CountDownLatch

    @Before
    @Throws(Exception::class)
    fun setUp() {
       service = ChannelsServiceImpl()
    }

    @Test
    fun getChannels() {
        doneSignal = CountDownLatch(1)

        service.getChannels(object : OnChannelsLoaded {
            override fun onLoaded(channels: List<Channel>) {
                assert(channels.isNotEmpty())

               doneSignal.countDown()
            }

            override fun onFailed() {
                doneSignal.countDown()
            }
        })

        try {
            doneSignal.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}