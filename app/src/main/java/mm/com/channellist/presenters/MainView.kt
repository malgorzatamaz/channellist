package mm.com.channellist.presenters

import mm.com.channellist.channels.model.Channel

interface MainView{
    fun onLoadStarted()
    fun onLoaded(channels: List<Channel>)
    fun onFailed()
}