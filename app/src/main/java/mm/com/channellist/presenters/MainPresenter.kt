package mm.com.channellist.presenters

interface MainPresenter {
    fun loadChannels()
}
