package mm.com.channellist.presenters

import mm.com.channellist.channels.listeners.OnChannelsLoaded
import mm.com.channellist.channels.model.Channel
import mm.com.channellist.channels.service.ChannelsService
import mm.com.channellist.channels.service.ChannelsServiceImpl
import java.lang.ref.WeakReference


class MainPresenterImpl : MainPresenter {
    var channelService: ChannelsService
    var mainView: WeakReference<MainView>


    constructor(mainView: MainView) {
        this.mainView = WeakReference(mainView)
        channelService = ChannelsServiceImpl()
    }

    override fun loadChannels() {
        mainView.get()?.onLoadStarted()

        channelService.getChannels(object : OnChannelsLoaded {
            override fun onLoaded(channels: List<Channel>) {
                mainView.get()?.onLoaded(channels)
            }

            override fun onFailed() {
                mainView.get()?.onFailed()
            }
        })

    }
}