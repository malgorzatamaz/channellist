package mm.com.channellist.channels.model

import com.google.gson.annotations.SerializedName

class DefaultThumbnail{
    @SerializedName("url")
    var url = ""

    @SerializedName("height")
    var height = -1

    @SerializedName("width")
    var width = -1
}