package mm.com.channellist.channels.model

import com.google.gson.annotations.SerializedName

class Thumbnails{
    @SerializedName("default")
    lateinit var default: DefaultThumbnail
}