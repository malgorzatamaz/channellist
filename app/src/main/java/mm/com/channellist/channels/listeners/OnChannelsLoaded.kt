package mm.com.channellist.channels.listeners

import mm.com.channellist.channels.model.Channel


interface OnChannelsLoaded{
    fun onLoaded(channels: List<Channel>)
    fun onFailed()
}