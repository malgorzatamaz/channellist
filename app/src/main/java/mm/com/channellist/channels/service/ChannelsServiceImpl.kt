package mm.com.channellist.channels.service

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import mm.com.channellist.channels.listeners.OnChannelsLoaded
import mm.com.channellist.channels.model.ChannelResponse
import mm.com.channellist.network.RetrofitClient
import okhttp3.ResponseBody

class ChannelsServiceImpl: ChannelsService {
    private var service: NetworkService? = null
    private val baseUrl = "http://api.norbsoft.com"

    constructor() {
        service = getService()
    }

    private fun getService(): NetworkService {
        return RetrofitClient.getClient(baseUrl)!!.create<NetworkService>(NetworkService::class.java)
    }

    override fun getChannels(listener: OnChannelsLoaded) {
        if (service != null)
            service!!.getChannels()
                    .subscribeOn(Schedulers.newThread())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe(object : DisposableObserver<ChannelResponse>() {
                        override fun onComplete() {

                        }

                        override fun onNext(t: ChannelResponse) {
                            if(t.channels != null)
                                listener.onLoaded(t.channels!!)
                            else
                                listener.onFailed()
                        }

                        override fun onError(e: Throwable) {
                            listener.onFailed()
                        }
                    })
    }
}