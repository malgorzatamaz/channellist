package mm.com.channellist.channels.model

import com.google.gson.annotations.SerializedName

class Details{
    @SerializedName("thumbnails")
    lateinit var thumbnails: Thumbnails

    @SerializedName("title")
    var title = ""

    @SerializedName("description")
    var description = ""
}