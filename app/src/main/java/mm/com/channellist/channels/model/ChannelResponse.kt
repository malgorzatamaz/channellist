package mm.com.channellist.channels.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ChannelResponse{
    @SerializedName("items")
    var channels: List<Channel>? = null
}