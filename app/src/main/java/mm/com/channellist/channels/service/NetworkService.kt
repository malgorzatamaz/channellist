package mm.com.channellist.channels.service

import io.reactivex.Observable
import mm.com.channellist.channels.model.ChannelResponse
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkService {
    @GET("/sciTube/v2/channels.json")
    fun getChannels(): Observable<ChannelResponse>
}