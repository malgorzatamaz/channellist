package mm.com.channellist.channels.service

import mm.com.channellist.channels.listeners.OnChannelsLoaded

interface ChannelsService{
    fun getChannels(listener: OnChannelsLoaded)
}