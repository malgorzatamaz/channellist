package mm.com.channellist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View.GONE
import android.view.View.VISIBLE
import kotlinx.android.synthetic.main.activity_main.*
import mm.com.channellist.channels.model.Channel
import mm.com.channellist.list.ChannelListAdapter
import mm.com.channellist.presenters.MainPresenter
import mm.com.channellist.presenters.MainPresenterImpl
import mm.com.channellist.presenters.MainView

class MainActivity : AppCompatActivity(), MainView {
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: ChannelListAdapter
    lateinit var presenter: MainPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenterImpl(this)

        val channels = ArrayList<Channel>()
        viewManager = LinearLayoutManager(this)
        viewAdapter = ChannelListAdapter(channels)

        channel_list_rv.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        presenter.loadChannels()
    }

    override fun onLoadStarted() {
        loader.visibility = VISIBLE
        error_tv.visibility = GONE
        channel_list_rv.visibility = GONE
    }

    override fun onLoaded(channels: List<Channel>) {
        loader.visibility = GONE
        error_tv.visibility = GONE
        channel_list_rv.visibility = VISIBLE

        updateList(channels)
    }

    private fun updateList(channels: List<Channel>) {
        viewAdapter.updateList(channels)
    }

    override fun onFailed() {
        loader.visibility = GONE
        error_tv.visibility = VISIBLE
        channel_list_rv.visibility = GONE
    }
}
