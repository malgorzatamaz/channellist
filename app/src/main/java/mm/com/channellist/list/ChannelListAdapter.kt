package mm.com.channellist.list

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import mm.com.channellist.channels.model.Channel
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.widget.LinearLayout
import mm.com.channellist.R


class ChannelListAdapter : RecyclerView.Adapter<ChannelViewHolder> {
    private var channels: ArrayList<Channel>

    constructor(channels: ArrayList<Channel>){
        this.channels = channels
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.channel_view, parent, false)
        val lp = v.layoutParams as RecyclerView.LayoutParams
        lp.height = parent.measuredHeight / 3
        v.layoutParams = lp
        return ChannelViewHolder(v)
    }

    override fun getItemCount(): Int {
        return channels.size
    }

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        holder.setValues(channels[position])
    }

    fun updateList(channels: List<Channel>) {
        this.channels.clear()
        this.channels.addAll(channels)

        notifyDataSetChanged()
    }

}