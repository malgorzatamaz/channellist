package mm.com.channellist.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.channel_view.view.*
import mm.com.channellist.channels.model.Channel

class ChannelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    lateinit var titleTv: TextView
    lateinit var descTv: TextView

    fun setValues(channel: Channel) {
        itemView.title_tv.text = channel.details?.title ?: ""
        itemView.desc_tv.text = channel.details?.description ?: ""

        if(channel.details != null)
            Picasso.get().load(channel.details!!.thumbnails.default.url).into(itemView.thumbnail_iv)
    }
}